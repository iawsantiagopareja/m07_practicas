<?php include_once('_header.php') ?>
<div class="container p-3">

    <div class="card">
        <div class="card-body">
            <form action="uploadManager.php" method="post" enctype="multipart/form-data">
                <div class="card-title text-center">Uppload Picture</div>
                <div class="mb-3">
                    <label for="title" class="form-label">title:</label>
                    <input type="text" class="form-control" id="title" name="title">
                </div>
                <div class="mb-3">
                    <label for="imagen" class="form-label">File:</label>
                    <input class="form-control" type="file" id="imagen" name="imagen">
                </div>
                <input class="btn btn-primary" type="submit" name="submit" value="Upload">
            </form>
        </div>
    </div>
</div>
<?php include_once('_footer.php') ?>