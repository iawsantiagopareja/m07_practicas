<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>practica galeria</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>

<body>
    <header class="mb-3">
        <nav class="navbar navbar-dark justify-content-center bg-dark pb-3">
            <div class="container">
                <h2 class="nav-item text-white">Image's Gallery</h2>
            </div>
        </nav>
    </header>
    <?php
        $upload = $_GET["upload"];
        $mensaje = "picture Upload";
        if ($upload == "success") {
            ?>
    <div class="container pt-3">
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <?php echo $mensaje?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    </div>
    <?php
        } elseif($upload == "error") {
            $mensaje= $_GET["msg"];
            ?>
    <div class="container p-3">
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <?php echo $mensaje?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    </div>
    <?php
        }
    ?>