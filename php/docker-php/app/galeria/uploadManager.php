<?php

define("RUTA","picture/");
define("NAME","imagen");
/*
* Función que se encarga de subir un archivo y moverlo a la carpeta /pictures
* que almacena todas las fotos.
* Return: Devuelve la ruta final del archivo.
*/

function uploadPicture(){
    $mensaje = "";
    $title = $_POST["title"];
    $fileName = $_FILES[NAME]["name"];
    $extension = pathinfo($fileName)["extension"];
    $extensiones = ["jpg","jpeg","git","png"];
    
    try {
        // veirifco si hay un fichero que subir
        if (empty($fileName)) { 
            throw new uploadError("Error: no subistes ninguna foto");
        }
        // verifico que sea una imagen
        if (!in_array($extension,$extensiones)) {
            throw new uploadError("Error: "  .$fileName ." no es una imagen");
        }
        // verifico que la imagen no exista en la carpeta 
        if (file_exists(RUTA .$fileName)) {
            throw new uploadError("Error: " .$fileName ." ya existe");
        }
        // verifico que se coloco titulo 
        if(empty($title)) {
            throw new uploadError("Error: no colocastes titulo a la imagen");
        }


        // si cumple todas las condiciones subo la foto a la carpeta 
        move_uploaded_file($_FILES[NAME]["tmp_name"], RUTA .$fileName);
        addPictureToFile(RUTA .$fileName,$title);
        $mensaje= RUTA .$fileName;
        header("Location: index.php?upload=success");

    } catch (UploadError $e) {
        header('Location: index.php?upload=error&msg=' . urlencode($e->getMessage()));

    }catch (Exception $e){
        header('Location: index.php?upload=error&msg=' . urlencode($e->getMessage()));
    }
    return $mensaje;
}

/*
* Función que se encarga de añadir al archivo fotos.txt el titulo y la ruta de la
* fotografía recien subida
* Entradas:
*       $file_uploaded: La ruta del archivo
*       $title_uploaded: El titulo del archivo
* Return: null
*/
function addPictureToFile($file_uploaded,$title_uploaded){
    $fichero = fopen("fotos.txt","a");
    fputs($fichero,$title_uploaded ."###" .$file_uploaded ."\n");
    fclose($fichero);
}

/*
* Clase personalizada extendida de Exception que utilizaremos para lanzar errores
* en la subida de archivos. Por ejemplo:
* throw new UploadError("Error: Please select a valid file format.")
*/
class UploadError extends Exception{}

uploadPicture();

?>