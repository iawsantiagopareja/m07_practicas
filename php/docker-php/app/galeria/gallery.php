<?php include_once('_header.php') ?>

<?php
include("Class/GalleryClass.php");
$rutaFotos = "fotos.txt";
$gallery = new Gallery($rutaFotos);
$pictures = $gallery->getGallery();
?>
<div class="container-fluid d-flex flex-wrap justify-content-evenly p-3">
<?php 
    if (empty($pictures)) {
        echo "no hay imagenes";
    }
    foreach ($pictures as $picture) {
?>
    <div class="card m-3" style="width: 400px; height: 300px">
        <img src="<?php echo $picture->fileName() ?>" class="card-img-top" style="height: 80%" alt="...">
        <div class="card-body">
            <p class="card-text"><?php echo $picture->title() ?></p>
        </div>
    </div>
<?php
        }
?>
</div>

<?php include_once('_footer.php') ?>