<?php

include("PictureClass.php");

class Gallery
{
    private $_gallery = [];
    private $_fileName;

  /*Constructor: Recibe la ruta del archivo fotos.txt*/
    function __construct($fileName){
            $this->_fileName=$fileName;
            $this->loadGallery();
    }

    /*
  *Recorre el archivo fotos.txt y para cada linea, crea un
  *elemento Picture que lo añade al atributo $_gallery[]
  */
    function loadGallery(){
      // abro el fichero donde alamceno los datos de las fots
        $fichero = fopen($this->_fileName,"r"); 
        // comienzo a recorrer las lineas del fichero 
        while(!feof($fichero)){
          // tomo las lineas del fichero 
            $linea = fgets($fichero);
            // separo el titulo de la ruta de las imagenes 
            $rutaYTitulo = explode("###",$linea);
            // creo el objeto picture 
            $picture = new Picture($rutaYTitulo[0],$rutaYTitulo[1]);
            // verifico que la linea no este vacia y que no tenga salto de linea
            if ($linea != "" && $linea != "\n") {
              array_push($this->_gallery,$picture); 
            }
                   
        }
        // cierro el fichero 
        fclose($fichero);
    }

    /*
  *Getters.
  */
    public function getGallery(){
        return $this->_gallery;
    }
}

?>