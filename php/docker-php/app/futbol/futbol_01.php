<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>pagina de futbol 1</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <style>

        html,body {
            width: 100%;
            height: 100%;
        }

        .contenedor {
            display: flex;
            justify-content: center;
            align-items: center;
            width: 100%;
            height: 50%;
        }

        .texto {
            width: 100px;
        }
    </style>
</head>
<body>
    <div class="contenedor">
        <div class="formulario">
            <div class="mb-3">hola, deberas colocar el numero de jugadores y de partidos</div>
            <form action="./futbol_02.php" method="post">
                <div class="mb-3">
                    <label for="numeroJugadores">coloca el numero de jugadores</label>
                    <input type="text" class="form-control texto" id="numeroJugadores" name="numeroJugadores">
                </div>
                <div class="mb-3">
                <label for="numeroPartidos">coloca el numero de partidos</label>
                    <input type="text" class="form-control texto" id="numeroPartidos" name="numeroPartidos">
                </div>
                <div class="mb-3">
                    <button type="submit" class="btn btn-primary">enviar</button>
                </div>
            </form>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
</body>
</html>