<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>futbol 2</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <style>
        .textoPartido {
            width: 40px;
        }
    </style>
</head>
<body>
    <?php
        // veo cuantos jugadores y partidos hay que ingresar  
        $numJugadores = $_POST["numeroJugadores"];
        $numPartidos = $_POST["numeroPartidos"];
        ?> 
    <div class="">
        <form action="./futbol_03.php" method="post">
            <table class="table table-hover">
              <thead>
                  <tr>
                      <th>jugador</th>
                      <?php
                      // bucle para ingresar la cabecera del cuadro con la cantidad de patidos
                        for($i = 0; $i < $numPartidos; $i++) {
                            ?> 
                            <th>partido <?php echo $i + 1?></th>
                            <?php
                        }
                      ?>
                  </tr>
              </thead>
              <?php 
              // bucle para imprimir los inputs donde se coloca los jugadores
                for($i = 0; $i < $numJugadores; $i++) {
                    ?> 
                    <tr>
                        <td>
                            <input type="text" name="jugadores[]">
                        </td>
                        <?php
                        // bucle para imprimir los inputs donde se coloca los num del jugador de cada partido 
                            for($j = 0; $j < $numPartidos; $j++) {
                                ?> 
                                <td>
                                    <input type="text" class="textoPartido" name="partidos[<?php echo $i ?>][]">
                                </td>
                                <?php
                            }
                        ?>
                    </tr>
                    <?php
                }
              ?>  
            </table>
            <button type="submit" class="btn btn-primary">enviar</button>
        </form>
    </div>
</body>
</html>