<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>futbol 3</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>
<body>
    <?php 
        // inicializo las var donde almaceno los arrays de jugadores y partidos
        // y verifico el numero de jugadores y partidos   
        $partidos = $_POST["partidos"];
        $jugadores = $_POST["jugadores"];
        $numPartidos = 0;
        $numJugadores = 0;
        // verifico que hay al menos un partido 
        if (isset($partidos)) {
            $numPartidos = count($partidos[0]);
        } 
        // verifico que al menos hay un jugador
        if(isset($jugadores)) {
            $numJugadores = count($jugadores);
        }
    ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>jugador</th>
                <?php
                    for($i = 0; $i < $numPartidos; $i++) {
                        ?> 
                        <th> partido <?php echo $i + 1 ?> </th>
                        <?php
                    }
                ?>
            </tr>
        </thead>
        <?php 
            for($i = 0; $i < $numJugadores; $i++) {
                ?> 
                <tr>
                    <td> <?php echo $jugadores[$i] ?></td>
                    <?php 
                        for($j = 0; $j < $numPartidos; $j++) {
                            ?>
                            <td><?php echo $partidos[$i][$j] ?></td>
                            <?php
                        }
                    ?>
                </tr>
                <?php
            }
        ?>
    </table>
</body>
</html>