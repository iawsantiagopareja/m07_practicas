<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>pagina inicio practicas M07</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

</head>

<body>
    <nav class="navbar navbar-dark justify-content-center bg-dark">
        <h2 class="nav-item text-white">pagina inicio practicas M07</h2>
    </nav>

    <div class="container pt-5">
        <div class="list-group">
                <a type="button" href="productos/prod_list_01.php" class="list-group-item list-group-item-action">practica productos</a>
                <a type="button" href="futbol/futbol_01.php" class="list-group-item list-group-item-action">practica furbol</a>
                <a type="button" href="practica_POO_I/ficha_alumno.php" class="list-group-item list-group-item-action">practica alumnos</a>
                <a type="button" href="galeria/index.php" class="list-group-item list-group-item-action">practica galeria</a>
        </div>
    </div>
</body>

</html>