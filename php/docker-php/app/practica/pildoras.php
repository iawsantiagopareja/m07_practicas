<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>pildoras</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <style>
        .pildoras {
            margin-top: 50px;
            padding-bottom: 20px;
            border-bottom: 1px solid black; 
        }
    </style>
</head>
<body>
    <?php 
        $valor1 = 4;
        $valor2 = 3;
        $nombre1 = "A";
        $nombre2 = "B";
    ?>
    <div class="container">
        <div class="pildoras row">
            <div>pildora 1</div>
            <div class="tabla container">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Operacion</th>
                        <th>valor</th>
                    </tr>
                </thead>
                <tr>
                    <td> <?php echo $nombre1; ?> </td>
                    <td> <?php echo $valor1; ?> </td>
                </tr>
                <tr>
                    <td> <?php echo $nombre2; ?> </td>
                    <td> <?php echo $valor2; ?> </td>
                </tr>
                <tr>
                    <td> <?php echo $nombre1 ."+" .$nombre2 ?> </td>
                    <td> <?php echo ($valor1 + $valor2) ?> </td>
                </tr>
                <tr>
                    <td> <?php echo $nombre1 ."-" .$nombre2 ?> </td>
                    <td> <?php echo $valor1 - $valor2 ?> </td>
                </tr>
                <tr>
                    <td> <?php echo $nombre1 ."*" .$nombre2 ?> </td>
                    <td> <?php echo $valor1 * $valor2 ?> </td>
                </tr>
                <tr>
                    <td> <?php echo $nombre1 ."/" .$nombre2 ?> </td>
                    <td> <?php echo round($valor1 / $valor2,2) ?> </td>
                </tr>
                <tr>
                    <td> <?php echo $nombre1 ."exp" .$nombre2 ?> </td>
                    <td> <?php echo $valor1 ** $valor2 ?> </td>
                </tr>
            </table> 
            </div>       
        </div>


        <?php 

        ?>
        <div class="pildoras row">
            <div>pildora 2</div>
            <form method="post" action="/practica/pildora2.php">
                <div class="mb-3">
                    <label class="form-label">coloca un numero</label>
                    <input type="text" class="form-control" id="numero1" name="numero1">
                </div>
                <div class="mb-3">
                    <label class="form-label">coloca otro numero</label>
                    <input type="text" class="form-control" id="numero2" name="numero2">
                </div>
                <div class="mb-3">
                    <select class="form-select" aria-label="Default select example" name="operacion">
                        <option selected>sumar</option>
                        <option>restar</option>
                        <option>multiplicar</option>
                        <option>dividir</option>
                        <option> exponente </option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>


        <div class="pildoras row">
            <div>pildora 3.1</div>
            <?php 
                $loteria = array(61,32,43,61);
                $numGanador = 61;
            ?>
            <div>cuantas veces aparece el numero 61 en el array:</div>
            <div>
                <?php 
                echo var_dump($loteria); 
                    $numElementosArray = count($loteria);
                    $numRepeticiones = 0;
                    for ($i=0; $i < $numElementosArray ; $i++) { 
                        if ($numGanador == $loteria[$i]) {
                            $numRepeticiones++;
                        }
                    }
                    echo "<br> el numero: " .$numGanador ." se repite " .$numRepeticiones;
                ?>
            </div>
        </div>


        <div class="pildoras row">
                <form action="/practica/pildora3.php" method="post">
                <div class="mb-3">pildora 3.2</div>
            <div class="mb-3">cual numero quieres saber cuantas veces se repite en el array</div>
                <div class="mb-3">
                <div>ingresa el numero:</div>
                    <input type="text" class="form-control" name="numero">
                </div>    
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
        </div>
    </div>
   

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
</body>
</html>