<?php

define("RUTA","picture/");

class UploadError extends Exception{}

class Upload {

    private $name;

/*
  * Constructor: Inicia la subida del archivo
  * Entrada:
  *   $name: Nombre del elemento del formulario del que gestionamos el $_FILE
  */
    function __construct($name) {
        $this->name = $name;
        if(isset($_FILES[$name])) $this->upload();
    }

    // nota: tengo que verificar tambien el tipo de archivo que sea una jpg. etc
    public function upload() {
        try {
            $fileName = $_FILES[$this->name]["name"];

            if(file_exists(RUTA .$fileName)) {
                throw new uploadError("ese archivo ya existe");
            }

            move_uploaded_file($_FILES[$this->name]["tmp_name"], RUTA .$fileName);
        
        } catch (UploadError $e){
            echo "Error de subida: " . $e->getMessage();
        } catch (Exception $e){
            echo $e->getMessage();
        }
    }


    public function getPath() {
        return RUTA . $_FILES[$this->name]["name"];
    }

}

?>