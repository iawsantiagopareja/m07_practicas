<?php

class Persona {

    private $nombre;
    private $apellido;
    private $fotografia;
    private $direccion;
    private $comentarios;
    private $listaMatricula;


 /*
  * Setters. Para añadir y modificar valores
  */
    public function setName($nombre){
        $this->nombre=$nombre;
    }

    public function setSurname($apellido){
        $this->apellido=$apellido;
    }

    public function setPicture($fotografia){
        $this->fotografia=$fotografia;
    }

    public function setAddress($direccion){
        $this->direccion=$direccion;
    }

    public function setComentarios($comentarios){
        $this->comentarios=$comentarios;
    }

    public function setListaMatricula($listaMatricula) {
        $this->listaMatricula=$listaMatricula;
    }

/*
  * Getters. Lo que quiere decir que los atributos de la clase son private
  */
    public function getName(){
        return $this->nombre;
    }

    public function getSurname(){
        return $this->apellido;
    }

    public function getPicture(){
        return $this->fotografia;
    }

    public function getAddress(){
        return $this->direccion;
    }

    public function getComentarios() {
        return $this->comentarios;
    }

    public function getListaMatricula() {
        return $this->listaMatricula;
    }

}

?>