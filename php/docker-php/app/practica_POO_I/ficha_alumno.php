<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Upload Person</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>
<body>
    <nav class="navbar navbar-dark justify-content-center bg-dark">
        <div class="container">
            <h2 class="nav-item text-white">Upload Person</h2>
        </div>
    </nav>

    <div class="container pt-3">
        <div class="card">
            <div class="card-body">
                <form action="./ficha_alumno_view.php" method="post" enctype="multipart/form-data">
                    <div class="mb-3">
                        <label for="nombre" class="form-label">Name:</label>
                        <input class="form-control" type="text" id="nombre" name="name">
                    </div>
                    <div class="mb-3">
                        <label for="surname" class="form-label">Surname: </label>
                        <input class="form-control" type="text" id="" name="surname" name="surname">
                    </div>
                    <div class="mb-3">
                        <label for="address" class="form-label">Address:</label>
                        <input class="form-control" type="text" id="address" name="address">
                    </div>
                    <div class="mb-3">
                        <label for="comments" class="form-label">comments: </label>
                        <textarea class="form-control" placeholder="Leave a comment here" id="comments" name="comments"
                            style="height: 100px"></textarea>
                    </div>
                    <div>
                        <label for="comments" class="form-label">matricula: </label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="M03" name="matricula[]">
                            <label class="form-check-label" for="inlineCheckbox1">M03</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="M07" name="matricula[]">
                            <label class="form-check-label" for="inlineCheckbox2">M07</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="M05" name="matricula[]">
                            <label class="form-check-label" for="inlineCheckbox3">M05</label>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="imagen" class="form-label">picture:</label>
                        <input class="form-control" type="file" id="imagen" name="imagen">
                    </div>
                    <div class="mb-3">
                        <input class="btn btn-primary" type="submit" name="submit" value="Upload">
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>