<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ficha alumkno view</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>
<style>
.contenedor {
    margin-top: 20px;
}
</style>

<body>
    <?php
        include('Class/UploadClass.php');
        include('Class/PersonaClass.php');
        // inicializo la clase persona
        $persona = new Persona();
        // incializo las varables donde almaceno los datos
        $persona->setPicture($_FILES["imagen"]["name"]);
        $persona->setName($_POST["name"]);
        $persona->setSurname($_POST["surname"]);
        $persona->setAddress($_POST["address"]);
        $persona->setComentarios($_POST["comments"]);
        $persona->setListaMatricula($_POST["matricula"]);
        $upload = new Upload("imagen");
        
    ?>
    <nav class="navbar navbar-dark justify-content-center bg-dark">
        <div class="container">
            <h2 class="nav-item text-white">Upload Person</h2>
        </div>
    </nav>

    <div class="container contenedor">
        <div class="card" style="width: 18rem;">
            <img src="<?php echo $upload->getPath()?>" class="card-img-top">
            <div class="card-body">
                <p class="card-text"><?php echo $persona->getName() ?></p>
                <p class="card-text"><?php echo $persona->getComentarios() ?> </p>
                <div>
                    <p class="card-text">Matricula:</p>
                    <ul class="list-group list-group-flush">
                        <?php 
                        $numMatriculas = count($persona->getListaMatricula());
                        for($i = 0; $i < $numMatriculas; $i++) {
                        ?>
                         <li class="list-group-item"><?php echo $persona->getListaMatricula()[$i] ?></li>
                         <?php
                        }
                        ?>                 
                    </ul>
                </div>
            </div>
            <div class="card-footer">
                <p class="text-muted"><?php echo $persona->getAddress() ?></p>
            </div>
        </div>
</body>

</html>