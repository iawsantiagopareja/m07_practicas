<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>product list</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <style>
    html,
    body {
        width: 100%;
        height: 100%;
    }

    .contenedor {
        margin-top: 30px;
    }

    .formulario {
        margin-left: 5%;
    }

    </style>
</head>

<body>
    <nav class="navbar navbar-dark justify-content-center bg-dark">
        <div class="container">
            <h2 class="nav-item text-white"> Product's List</h2>
        </div>
    </nav>

    <div class="container-fluid contenedor">
        <div class="card">
            <div class="card-body">
                <form class="row g-3 formulario" method="POST" action="./prod_list_02.php">
                    <div class="col-auto justify-content-end">
                        <input type="text" readonly class="form-control-plaintext text-end" 
                            value="numero de producto">
                    </div>
                    <div class="col-auto">
                        <input type="number" class="form-control" id="numeroDeProducto"  name="numeroDeProductos">
                    </div>
                    <div class="col-auto">
                        <button type="submit" class="btn btn-primary mb-3">send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>

</html>