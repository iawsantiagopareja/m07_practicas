<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product list 2</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <style>
    table {
        width: 900px;
    }

    tr {
        display: flex;
    }

    td {
        display: flex;
        width: 50%;
        align-items: center;
        justify-content: center;
        padding: 10px;
    }

    .contenedor {
        margin-top: 30px;
    }

    .boton {
        margin-left: 100px;
    }
    </style>

</head>

<body>
    <nav class="navbar navbar-dark justify-content-center bg-dark">
        <div class="container">
            <h2 class="nav-item text-white"> Product's List</h2>
        </div>
    </nav>
    <?php
        $numProduct = $_POST["numeroDeProductos"];
    ?>

    <div class="container-fluid contenedor">
        <div class="card">
            <div class="card-body d-flex justify-content-center">
                <form action="./prod_list_03.php" method="post">
                    <table>
                        <?php
                        for($i = 0; $i < $numProduct; $i++) {
                            ?>
                        <tr>
                            <td>
                                <div class="col-auto ">
                                    <input type="text" readonly class="form-control-plaintext text-end p-3"
                                        value="Product Name">
                                </div>
                                <div class="col-auto">
                                    <input type="text" class="form-control" name="productName[]">
                                </div>
                            </td>
                            <td>
                                <div class="col-auto ">
                                    <input type="text" readonly class="form-control-plaintext text-end p-3"
                                        value="price">
                                </div>
                                <div class="col-auto">
                                    <input type="text" class="form-control" name="price[]">
                                </div>
                            </td>
                        </tr>
                        <?php
                        }
                    ?>
                    </table>
                    <div class="col-auto boton">
                        <button type="submit" class="btn btn-primary mb-3">send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>