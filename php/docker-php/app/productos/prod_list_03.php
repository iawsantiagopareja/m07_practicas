<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product list 3</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>

<body>
    <nav class="navbar navbar-dark justify-content-center bg-dark">
        <div class="container">
            <h2 class="nav-item text-white"> Product's List</h2>
        </div>
    </nav>
    <?php
    // inicializo las var donde almaceno los nombres y precios de los productos
        $productName = $_POST["productName"];
        $productPrice = $_POST["price"];
        $numProduct = count($productName);
    ?>

    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Product Name</th>
                <th>price</th>
            </tr>
        </thead>
        <?php
        // variable del numero de producto
            $num = 1;
        // comienzo el bucle para recorrer todos los productos
            for($i = 0; $i < $numProduct; $i++) {
                ?>
        <tr>
            <?php
                // verifico si se escribio en la casilla 
                    if (empty($productName[$i])){
                        // si esta vacio resto un numero de producto
                        $num--;
                   ?>
            <td colspan="3">product not inserted</td>
            <?php
                    } else{
                        ?>
            <td><?php echo $num?></td>
            <td><?php echo $productName[$i] ?> </td>
            <td> <?php echo $productPrice[$i] ?> </td>
            <?php
            }
            ?>
        </tr>
        <?php
             $num++;
            }
        ?>
    </table>
</body>

</html>