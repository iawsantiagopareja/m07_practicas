<? include_once('_header.php') ?>

    <div class="container pt-3">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Upload picture</h3>
                <form action="uploadManager.php" method="post" enctype="multipart/form-data">
                    <div class="mb-3">
                        <label for="titulo">Title:</label>
                        <input class="form-control" type="text" id="title">
                    </div>
                    <div class="mb-3">
                        <label for="picture" class="form-label">picture:</label>
                        <input class="form-control" type="file" id="picture">
                    </div>
                </form>
            </div>
        </div>
    </div>

<? include_once('_footer.php') ?>