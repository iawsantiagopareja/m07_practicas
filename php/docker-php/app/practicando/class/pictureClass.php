<?php

    class Picture {

        private $title;
        private $fileName;

        function __construct($title,$fileName){
            $this->title = $title;
            $this->fileName = $fileName;
        }

        public function getTitle() {
            return $this->title;
        }

        public function getFileName() {
            return $this->fileName;
        }


    }
?>